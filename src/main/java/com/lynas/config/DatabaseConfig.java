package com.lynas.config;

import com.lynas.model.*;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by sazzad on 9/7/15
 */
@Configuration
@EnableTransactionManagement
@PropertySource(value = "classpath:application.properties")
public class DatabaseConfig {

    @Autowired
    private ApplicationContext appContext;
    @Value("${db.server.address}")
    private String serverAddress;
    @Value("${db.port}")
    private int port;
    @Value("${db.name}")
    private String dbName;
    @Value("${db.user.name}")
    private String userName;
    @Value("${db.password}")
    private String password;


    @Bean(name = "DataSource")
    public HikariDataSource dataSourceWinMac() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        dataSource.addDataSourceProperty("databaseName", dbName);
        dataSource.addDataSourceProperty("portNumber", port);
        dataSource.addDataSourceProperty("serverName", serverAddress);
        dataSource.addDataSourceProperty("user", userName);
        dataSource.addDataSourceProperty("password", password);
        return dataSource;
    }


    @Bean
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager manager = new HibernateTransactionManager();
        manager.setSessionFactory(hibernate5SessionFactoryBean().getObject());
        return manager;
    }

    @Bean(name = "sessionFactory")
    public LocalSessionFactoryBean hibernate5SessionFactoryBean() {
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(appContext.getBean(DataSource.class));
        localSessionFactoryBean.setAnnotatedClasses(
                AppUser.class,
                Organization.class,
                Book.class,
                Account.class,
                AccountTransaction.class,
                Stock.class,
                StockTransaction.class
        );

        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        //properties.put("hibernate.current_session_context_class","thread");
        properties.put("hibernate.hbm2ddl.auto", "update");

        localSessionFactoryBean.setHibernateProperties(properties);
        return localSessionFactoryBean;
    }
}
