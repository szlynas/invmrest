package com.lynas.model.util

import java.io.Serializable

/**
 * Created by sazzad on 11/16/16
 */

class AuthenticationResponse() : Serializable {
    var token: String? = null

    constructor(token: String) : this(){
        this.token = token
    }

}

class EndPoint() : Serializable {
    var root: String? = null
    var method: String? = null
    var path: String? = null
    var reqBody: String? = null

    constructor(root: String, method: String, path: String, reqBody: String) : this() {
        this.root = root
        this.method = method
        this.path = path
        this.reqBody = reqBody
    }
}

class SuccessJson() : Serializable {
    var success: Boolean? = null

    constructor(success: Boolean) : this(){
        this.success = success
    }
}

class BalanceSheet() : Serializable {
    var name: String? = null
    var totals: Double? = null

    constructor(name: String,totals: Double) : this(){
        this.name = name
        this.totals = totals
    }
}