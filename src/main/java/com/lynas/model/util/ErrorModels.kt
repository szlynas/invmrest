package com.lynas.model.util

import java.io.Serializable

/**
 * Created by sazzad on 11/16/16
 */


class ErrorResponse() : Serializable {
    var originPoint: Any? = null
    var message: String? = null
    var receivedObject: Any? = null

    constructor(originPoint: Any, message: String, receivedObject: Any) : this() {
        this.originPoint = originPoint
        this.message = message
        this.receivedObject = receivedObject

    }
}
