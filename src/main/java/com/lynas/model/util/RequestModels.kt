package com.lynas.model.util

import java.io.Serializable

/**
 * Created by sazzad on 11/16/16
 */

class AuthenticationRequest() : Serializable {
    var username: String? = null
    var password: String? = null

    constructor(username: String, password: String) : this() {
        this.username = username
        this.password = password
    }
}

class NewPassword() : Serializable {
    var password: String? = null

    constructor(password: String) : this() {
        this.password = password
    }
}

class SignUpRequest() : Serializable {
    var organizationName: String? = null
    var username: String? = null
    var password: String? = null

    constructor(organizationName: String, username: String, password: String) : this() {
        this.organizationName = organizationName
        this.username = username
        this.password = password
    }
}

class AccountJson() : Serializable {
    var accountId: Long? = null
    var bookId: Long? = null
    var organizationId: Long? = null

    constructor(accountId: Long, bookId: Long, organizationId: Long) : this() {
        this.accountId = accountId
        this.bookId = bookId
        this.organizationId = organizationId
    }
}

class AccountUpdateJson() : Serializable {

    var accountId: Long? = null
    var accountNewName: String? = null
    var bookId: Long? = null
    var organizationId: Long? = null

    constructor(accountId: Long, accountNewName: String, bookId: Long, organizationId: Long) : this() {
        this.accountId = accountId
        this.accountNewName = accountNewName
        this.bookId = bookId
        this.organizationId = organizationId
    }
}

class StockJson() : Serializable {
    var stockId: Long? = null
    var bookId: Long? = null
    var organizationId: Long? = null

    constructor(stockId: Long, bookId: Long, organizationId: Long) : this() {
        this.stockId = stockId
        this.bookId = bookId
        this.organizationId = organizationId
    }
}

class StockTransactionJson() : Serializable {
    var stockTransactionId: Long? = null
    var stockId: Long? = null
    var bookId: Long? = null
    var organizationId: Long? = null

    constructor(stockTransactionId: Long, stockId: Long, bookId: Long, organizationId: Long) : this() {
        this.stockTransactionId = stockTransactionId
        this.stockId = stockId
        this.bookId = bookId
        this.organizationId = organizationId


    }


}