package com.lynas;

/**
 * Created by LynAs on 23-Mar-16
 */
public class AppConstant {
    public static final String tokenHeader = "X-Auth-Token";
    public static final String DEBIT="DEBIT",CREDIT="CREDIT",ADD="ADD",SELL="SELL";
    public static final String ENVIRONMENT= "WINMAC";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
}
