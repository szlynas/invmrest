package com.lynas.controller;

import com.lynas.Util;
import com.lynas.exception.AppExceptionHandler;
import com.lynas.model.Stock;
import com.lynas.model.util.StockJson;
import com.lynas.model.util.SuccessJson;
import com.lynas.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by LynAs on 24-Mar-16
 */
@RestController
@RequestMapping("stock")
public class StockController extends AppExceptionHandler {

    private final Util util;
    private final StockService stockService;

    @Autowired
    public StockController(Util util, StockService stockService) {
        this.util = util;
        this.stockService = stockService;
    }

    @PostMapping("/book_id/{book_id}")
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE')")
    public ResponseEntity<?> create(@RequestBody Stock stock, @PathVariable long book_id) {
        stock.setBook(util.getBookOrThrowError(book_id, util.getOrgIdFromToken()));
        stock.setId(stockService.post(stock));
        return util.respOK(stock);
    }

    @GetMapping("/list/book_id/{book_id}")
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE')")
    public ResponseEntity<?> getStockList(@PathVariable long book_id) {
        util.getBookOrThrowError(book_id, util.getOrgIdFromToken());
        return util.respOK(stockService.getStockListByBookOrganization(book_id, util.getOrgIdFromToken()));
    }

    @DeleteMapping
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE')")
    public ResponseEntity<?> delete(@RequestBody StockJson stockJson) {
        util.getBookOrThrowError(stockJson.getBookId(), util.getOrgIdFromToken());
        stockService.customDelete(stockJson.getStockId());
        return util.respOK(new SuccessJson(true));
    }
}
