package com.lynas.controller;

import com.lynas.Util;
import com.lynas.exception.AppExceptionHandler;
import com.lynas.model.Account;
import com.lynas.model.util.AccountJson;
import com.lynas.model.util.AccountUpdateJson;
import com.lynas.model.util.SuccessJson;
import com.lynas.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by LynAs on 24-Mar-16
 */
@RestController
@RequestMapping("account")
public class AccountController extends AppExceptionHandler {

    @Autowired
    private Util util;
    @Autowired
    private AccountService accountService;

    @PostMapping("/book_id/{book_id}")
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE')")
    public ResponseEntity<?> create(@PathVariable long book_id,@RequestBody Account account) {
        account.setBook(util.getBookOrThrowError(book_id, util.getOrgIdFromToken()));
        account.setId(accountService.post(account));
        return util.respOK(account);
    }

    @GetMapping(value = "/list/book_id/{book_id}")
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE')")
    public ResponseEntity<?> getAccountList(@PathVariable long book_id) {
        return util.respOK(accountService.getAccountListByBook(util.getBookOrThrowError(book_id, util.getOrgIdFromToken())));
    }


    @DeleteMapping
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE')")
    public ResponseEntity<?> deleteAccount(@RequestBody AccountJson accountJson) {
        util.getBookOrThrowError(accountJson.getBookId(), util.getOrgIdFromToken());
        accountService.customDelete(accountJson.getAccountId());
        System.out.println(accountJson.toString());
        return util.respOK(new SuccessJson(true));
    }


    @PatchMapping
    @PreAuthorize("hasAnyRole('ROLE_EMPLOYEE')")
    public ResponseEntity<?> updateAccount(@RequestBody AccountUpdateJson accountUpdateJson) {
        util.getBookOrThrowError(accountUpdateJson.getBookId(), util.getOrgIdFromToken());
        Account account = accountService.get(accountUpdateJson.getAccountId());
        account.setName(accountUpdateJson.getAccountNewName());
        accountService.patch(account);
        return util.respOK(account);
    }


}
